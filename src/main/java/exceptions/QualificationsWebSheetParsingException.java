package exceptions;

public class QualificationsWebSheetParsingException extends Exception {
	
	private static final long serialVersionUID = 634838644838365533L;
	

	public QualificationsWebSheetParsingException() {
		super();
	}
	
	
	public QualificationsWebSheetParsingException(final String message) {
		super(message);
	}
	
}