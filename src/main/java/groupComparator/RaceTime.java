package groupComparator;

public class RaceTime {
	
	private static final String STR_EMPTY_STRING = "";
	private static final String STR_SPACE = " ";
	private static final String STR_KEY_CHAR_DELTA_TIME = "+";
	private static final String STR_HOUR_DELIMITER = "h";
	private static final String STR_ZERO_MINUTES = "00:";
	private static final String STR_ZERO_STRING = "0";
	private static final String STR_MINUTES_DELIMITER = ":";
	private static final String STR_SECONDS_DELIMITER = ".";
	private static final String STR_DEFAULT_STRING = "--";

	private String relativeRaceTime;
	private String absoluteRaceTime;
	private Integer absoluteRaceTimeInMilliseconds;
	private Integer relativeRaceTimeInMilliSeconds;

	
	public RaceTime() {
		relativeRaceTime = STR_DEFAULT_STRING;
		absoluteRaceTime = STR_DEFAULT_STRING;
		absoluteRaceTimeInMilliseconds = 0;
		relativeRaceTimeInMilliSeconds = 0;
	}
	
	
	public Integer calculateRaceTimeFromAbsoluteRaceTimeString() {
		
		Integer result = 0;
		
		int hours = 0;
		int minutes = 0;
		double seconds = 0;
		String raceTime = this.absoluteRaceTime.replaceAll(STR_SPACE, STR_EMPTY_STRING);
		
		final String[] splittedStringHours = raceTime.split(STR_HOUR_DELIMITER);
		if (splittedStringHours != null && splittedStringHours.length == 2) {
			hours = Integer.valueOf(splittedStringHours[0]);
			raceTime = splittedStringHours[1];
		}
		
		final String[] splittedStringMinutes = raceTime.split(STR_MINUTES_DELIMITER);
		if (splittedStringMinutes != null && splittedStringMinutes.length == 2) {
			minutes = Integer.valueOf(splittedStringMinutes[0]);
			seconds = Double.valueOf(splittedStringMinutes[1]);
		}
		
		result = 1000 * (3600 * hours + 60 * minutes) + (int)(Math.round(1000 * seconds));
		
		return result;
	}
	
	
	public Integer calculateRaceTimeFromRelativeRaceTimeString() {
		
		Integer result = 0;
		
		if (!this.relativeRaceTime.contains(STR_KEY_CHAR_DELTA_TIME) && this.relativeRaceTimeInMilliSeconds.equals(0)) {
			// leader time race, we don't do anything
			setRelativeRaceTime(STR_DEFAULT_STRING);
			return result;
		}
		
		String deltaTime = this.relativeRaceTime.substring(2);
		
		int hours = 0;
		int minutes = 0;
		double seconds = 0d;
		
		final String[] splittedStringHours = deltaTime.split(STR_HOUR_DELIMITER);
		if (splittedStringHours != null && splittedStringHours.length == 2) {
			hours = Integer.valueOf(splittedStringHours[0]);
			deltaTime = splittedStringHours[1];
		} else if (splittedStringHours.length == 1) {
			deltaTime = splittedStringHours[0];
		}
		
		final String[] splittedStringMinutes = deltaTime.split(STR_MINUTES_DELIMITER);
		if (splittedStringMinutes != null && splittedStringMinutes.length == 2) {
			minutes = Integer.valueOf(splittedStringMinutes[0]);
			seconds = Double.valueOf(splittedStringMinutes[1]);
		} else if (splittedStringMinutes.length == 1) {
			deltaTime = splittedStringMinutes[0];
		}
		
		if (seconds == 0d) {
			seconds = Double.valueOf(deltaTime);
		}
		
		result = 1000 * (3600 * hours + 60 * minutes) + (int)(Math.round(seconds * 1000));
		
		return result;
	}
	
	
	public String buildCleanRaceTime(final int raceTimeInMilliseconds) {
		
		String result = null;
		
		int hours = raceTimeInMilliseconds / 3600 / 1000;
		int minutes = (raceTimeInMilliseconds - hours * 3600 * 1000) / (60 * 1000);
		int seconds = (raceTimeInMilliseconds - hours * 3600 * 1000 - minutes * 60 * 1000) / 1000;
		int milliseconds = raceTimeInMilliseconds - 1000 * (hours * 3600 + minutes * 60 + seconds);
		
		if (raceTimeInMilliseconds == 0) {
			result = STR_DEFAULT_STRING;
			return result;
		}
		
		final StringBuilder raceTime = new StringBuilder();
		
		if (hours > 0) {
			raceTime.append(hours);
			raceTime.append(STR_HOUR_DELIMITER);
		}
		if (hours > 0 && minutes == 0) {
			raceTime.append(STR_ZERO_MINUTES);
		} else {
			if (hours > 0 && minutes > 0 && minutes < 10) {
				raceTime.append(STR_ZERO_STRING);
			}
			if (hours > 0 || minutes > 0) {
				raceTime.append(minutes);
				raceTime.append(STR_MINUTES_DELIMITER);
			}
		}
		if (seconds < 10 && (hours > 0 || minutes > 0)) {
			raceTime.append(STR_ZERO_STRING);
		}
		raceTime.append(seconds);
		
		raceTime.append(STR_SECONDS_DELIMITER);
		if (milliseconds < 100) {
			raceTime.append(STR_ZERO_STRING);
		}
		if (milliseconds < 10) {
			raceTime.append(STR_ZERO_STRING);
		}
		raceTime.append(milliseconds);
		
		return raceTime.toString();
	}
	
	
	@Override
	public String toString() {
		
		return "RaceTime [absoluteRaceTime=" + absoluteRaceTime  + ", absoluteRaceTimeInMilliseconds=" 
				+ absoluteRaceTimeInMilliseconds + ", relativeRaceTime=" + relativeRaceTime + ", relativeRaceTimeInMilliSeconds="
				+ relativeRaceTimeInMilliSeconds + "]";
	}


	public String getRelativeRaceTime() {
		return relativeRaceTime;
	}

	
	public void setRelativeRaceTime(String relativeRaceTime) {
		this.relativeRaceTime = relativeRaceTime;
	}

	
	public String getAbsoluteRaceTime() {
		return absoluteRaceTime;
	}

	
	public void setAbsoluteRaceTime(String absoluteRaceTime) {
		this.absoluteRaceTime = absoluteRaceTime;
	}
	
	
	public Integer getAbsoluteRaceTimeInMilliseconds() {
		return absoluteRaceTimeInMilliseconds;
	}


	public void setAbsoluteRaceTimeInMilliseconds(Integer absoluteRaceTimeInMilliseconds) {
		this.absoluteRaceTimeInMilliseconds = absoluteRaceTimeInMilliseconds;
	}


	public Integer getRelativeRaceTimeInMilliseconds() {
		return relativeRaceTimeInMilliSeconds;
	}


	public void setRelativeRaceTimeInMilliseconds(Integer relativeRaceTimeInMilliseconds) {
		this.relativeRaceTimeInMilliSeconds = relativeRaceTimeInMilliseconds;
	}
}
