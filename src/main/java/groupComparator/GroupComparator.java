package groupComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import data.exporter.ExporterCSV;
import data.importer.GroupList;
import data.importer.RaceResult;
import data.importer.RawRaceResultEntry;
import utils.ClassifierUtils;

public class GroupComparator {
	
	public static final String URL_QUALIFICATIONS_STATISTICS = "https://gpro.net/fr/QualifyStats.asp";
	public static final String URL_RACE_RESULTS_ROOT = "https://gpro.net/fr/RaceSummary.asp";
	
	public static final int INT_JSOUP_CONNEXION_TIMEOUT_LIMIT = 30 * 1000;
	
	private static final Integer MAX_NUMBER_OF_RACE_PER_SEASON = 17;
	private static final Integer MAX_NUMBER_OF_ELITE_GROUPS = 1;
	private static final Integer MAX_NUMBER_OF_MASTER_GROUPS = 5;
	private static final Integer MAX_NUMBER_OF_PRO_GROUPS = 25;
	private static final Integer MAX_NUMBER_OF_AMATEUR_GROUPS = 125;
	private static final Integer MAX_NUMBER_OF_ROOKIE_GROUPS = 416;
	

	private Integer currentSeason;
	private Integer currentRace;
	private List<Integer> groups;
	
	
	public GroupComparator() {
		
		groups = new ArrayList<>();
		currentSeason = 0;
		currentRace = 0;
	}
	
	
	private void buildGroupListFromQualificationsStats() {
		
		final GroupList groupList = new GroupList();
		groupList.importNumberOfGroupsByLeague();
		
		final Integer elite = groupList.getCountOfEliteGroup();
		final Integer master = groupList.getCountOfMasterGroup();
		final Integer pro = groupList.getCountOfProGroup();
		final Integer amateur = groupList.getCountOfAmateurGroup();
		final Integer rookie = groupList.getCountOfRookieGroup();
		
		groups.add(elite);
		groups.add(master);
		groups.add(pro);
		groups.add(amateur);
		groups.add(rookie);
	}
	
	
	private void buildGroupListFromDefaultValue() {
		
		groups.add(MAX_NUMBER_OF_ELITE_GROUPS);
		groups.add(MAX_NUMBER_OF_MASTER_GROUPS);
		groups.add(MAX_NUMBER_OF_PRO_GROUPS);
		groups.add(MAX_NUMBER_OF_AMATEUR_GROUPS);
		groups.add(MAX_NUMBER_OF_ROOKIE_GROUPS);
	}
	
	
	private void getMetaDataFromRaceResults() {
		
		final RaceResult raceResult = new RaceResult();
		raceResult.importCurrentMetaData();
		currentSeason = raceResult.getCurrentSeason();
		currentRace = raceResult.getCurrentRace();
	}
	
	
	private List<Map<String, List<RawRaceResultEntry>>> getDataFromRaceResults(final List<Integer> inputs) {
		
		System.out.println("[" + new Date() + "] " + "Starting to import race results from GPRO...");
		
		final Integer expectedSeason = inputs.get(0);
		final Integer expectedRace = inputs.get(1);
		
		final RaceResult raceResult = new RaceResult();
		final List<Map<String, List<RawRaceResultEntry>>> rawClassificationPerDivisionAndGroup = raceResult.importRaceResults(expectedSeason, expectedRace, groups);
	
		System.out.println("[" + new Date() + "] " + "Race results import from GPRO finished!");
		
		return rawClassificationPerDivisionAndGroup;
	}
	
	
	private List<Integer> manageInputs(final String[] args) {
		
		System.out.print("[" + new Date() + "] " + "Inputs managing...");
		
		Integer expectedSeason = null;
		Integer expectedRace = null;
		
		if (args.length >= 2) {
			
			expectedSeason = Integer.valueOf(args[0]);
			expectedRace = Integer.valueOf(args[1]);
			
			if (expectedRace > MAX_NUMBER_OF_RACE_PER_SEASON) {
				expectedRace = MAX_NUMBER_OF_RACE_PER_SEASON;
			}
			
			if (expectedSeason > currentSeason || expectedSeason.equals(currentSeason) && expectedRace > currentRace) {
				
				expectedSeason = currentSeason;
				expectedRace = currentRace;
			}
		} else if (args.length == 1) {
			
			expectedSeason = Integer.valueOf(args[0]);
			
			if (expectedSeason < currentSeason) {
				
				expectedRace = MAX_NUMBER_OF_RACE_PER_SEASON;
				
			} else {
				
				expectedSeason = currentSeason;
				expectedRace = currentRace;
			}
		} else if (args.length == 0) {
			
			expectedSeason = currentSeason;
			expectedRace = currentRace;
		}
		
		final List<Integer> inputs = new ArrayList<>();
		inputs.add(expectedSeason);
		inputs.add(expectedRace);
		
		System.out.println("    Results will be taken from season " + expectedSeason + ", race " + expectedRace + ".");
		
		return inputs;
	}
	
	
	private void initializeGroupStructure(final List<Integer> inputs) {
		
		System.out.print("[" + new Date() + "] " + "Initializing group structure...");
		
		final Integer expectedSeason = inputs.get(0);
		
		if (expectedSeason.equals(currentSeason)) {
			
			buildGroupListFromQualificationsStats();
			
		} else {
			
			buildGroupListFromDefaultValue();
		}
		
		System.out.println("   done!");
	}
	
	
	private List<RaceResultEntry> buildCleanRaceSummaryForAllEntries(final List<Map<String, List<RawRaceResultEntry>>> rawResultsPerDivision) {
		System.out.print("[" + new Date() + "] Cleaning data got from GPRO...");
		final RaceSummaryByDivision raceSummaryByDivision = new RaceSummaryByDivision();
		raceSummaryByDivision.setRawRaceResults(rawResultsPerDivision);
		final List<RaceResultEntry> raceResults = raceSummaryByDivision.cleanResults();
		System.out.println("   done!");
		return raceResults;
	}
	
	
	public void buildStats(final String[] args) {
		
		getMetaDataFromRaceResults();
		final List<Integer> correctedInputs = manageInputs(args);
		initializeGroupStructure(correctedInputs);
		final List<Map<String, List<RawRaceResultEntry>>> rawClassificationPerDivisionAndGroup = getDataFromRaceResults(correctedInputs);
		final List<RaceResultEntry> fullRaceResults = buildCleanRaceSummaryForAllEntries(rawClassificationPerDivisionAndGroup);
		
		summarizePositions(fullRaceResults, correctedInputs);
	}
	
	
	private String buildFilePath(final List<Integer> correctedInputs) {
		
		final int season = correctedInputs.get(0);
		final int race = correctedInputs.get(1);
		String filePath = "./Season";
		
		if (season < 100) {
			filePath += "0";
		}
		if (season < 10) {
			filePath += "0";
		}
		filePath += season + "/GP";
		
		if (race < 10) {
			filePath += "0";
		}
		filePath += race + "/";
		
		return filePath;
	}
	
	
	public void exportAllManagersCSV(final List<RaceResultEntry> fullRaceResults, final String filePath) {
		
		final ExporterCSV exporter = new ExporterCSV();
		exporter.setRaceResults(fullRaceResults);
		exporter.writeAllManagersCSV(filePath + "all_managers.csv");
	}
	
	
	private final List<RaceResultEntry> extractPosition(final List<RaceResultEntry> fullRaceResults, final int position) {
		
		final List<RaceResultEntry> result = new ArrayList<>();
		
		int masters = 0;
		int pros = 0;
		int amateurs = 0;
		int rookies = 0;
		for (final RaceResultEntry raceResult : fullRaceResults) {
			final int levelDivision = raceResult.getGroup().getLevelDivision();
			final int groupNumber = raceResult.getGroup().getGroupNumber();
			switch(levelDivision) {
			case 2:
				if (groupNumber > masters) masters = groupNumber;
				break;
			case 3:
				if (groupNumber > pros) pros = groupNumber;
				break;
			case 4:
				if (groupNumber > amateurs) amateurs = groupNumber;
				break;
			case 5:
				if (groupNumber > rookies) rookies = groupNumber;
				break;
			}
		}
		
		// get race times for each group
		for (final RaceResultEntry raceResult : fullRaceResults) {
			
			if (raceResult.getRacePosition() == position) {
				
				final RaceResultEntry groupEntry = new RaceResultEntry();
				groupEntry.setGroup(raceResult.getGroup());
				
				int milliseconds = raceResult.getRaceTime().getAbsoluteRaceTimeInMilliseconds();
				final RaceTime groupRaceTime = new RaceTime();
				groupRaceTime.setAbsoluteRaceTimeInMilliseconds(milliseconds);
				groupRaceTime.setAbsoluteRaceTime(groupRaceTime.buildCleanRaceTime(milliseconds));
				groupEntry.setRaceTime(groupRaceTime);
				
				groupEntry.setCountry(raceResult.getCountry());
				groupEntry.setManager(raceResult.getManager());
				groupEntry.setNumberOfPits(raceResult.getNumberOfPits());
				groupEntry.setRaceEvent(raceResult.getRaceEvent());
				groupEntry.setTyres(raceResult.getTyres());
				
				result.add(groupEntry);
			}
		}
		
		// fill missing race times if necessary
		final int[] numberOfGroups = {masters, pros, amateurs, rookies};
		for (int division = 2; division <= 5; division++) {
			for (int groupNumber = 1; groupNumber <= numberOfGroups[division - 2]; groupNumber++) {
				
				boolean found = false;
				for (final RaceResultEntry raceResult : result) {
					if (raceResult.getGroup().getLevelDivision() == division && raceResult.getGroup().getGroupNumber() == groupNumber) {
						found = true;
						break;
					}
				}
				
				if (!found) {
					final RaceResultEntry raceResult = new RaceResultEntry();
					final Group group = new Group();
					final RaceTime raceTime = new RaceTime();
					
					group.setLevelDivision(division);
					group.setGroupNumber(groupNumber);
					switch(division) {
					case 2:
						group.setDivision(Group.Division.MASTER);
						group.setDivisionFullName("Master - " + groupNumber);
						break;
					case 3:
						group.setDivision(Group.Division.PRO);
						group.setDivisionFullName("Pro - " + groupNumber);
						break;
					case 4:
						group.setDivision(Group.Division.AMATEUR);
						group.setDivisionFullName("Amateur - " + groupNumber);
						break;
					case 5:
						group.setDivision(Group.Division.ROOKIE);
						group.setDivisionFullName("Rookie - " + groupNumber);
						break;
					}
					raceTime.setAbsoluteRaceTime("4h00:00.000");
					raceTime.setAbsoluteRaceTimeInMilliseconds(raceTime.calculateRaceTimeFromAbsoluteRaceTimeString());
					raceResult.setGroup(group);
					raceResult.setRaceTime(raceTime);
					
					result.add(raceResult);
				}
			}
		}
		
		return result;
	}
	
	
	public void summarizePositions(final List<RaceResultEntry> fullRaceResults, final List<Integer> correctedInputs) {
		
		System.out.print("[" + new Date() + "] " + "Extracting race times for each expected positions...");
		final List<RaceResultEntry> p1 = extractPosition(fullRaceResults, 1);
		final List<RaceResultEntry> p2 = extractPosition(fullRaceResults, 2);
		final List<RaceResultEntry> p3 = extractPosition(fullRaceResults, 3);
		final List<RaceResultEntry> p4 = extractPosition(fullRaceResults, 4);
		final List<RaceResultEntry> p8 = extractPosition(fullRaceResults, 8);
		final List<RaceResultEntry> p9 = extractPosition(fullRaceResults, 9);
		final List<RaceResultEntry> p15 = extractPosition(fullRaceResults, 15);
		final List<RaceResultEntry> p16 = extractPosition(fullRaceResults, 16);
		final List<RaceResultEntry> p25 = extractPosition(fullRaceResults, 25);
		final List<RaceResultEntry> p26 = extractPosition(fullRaceResults, 26);
		System.out.println("   done!");
		
		System.out.print("[" + new Date() + "] " + "Classifying groups by race times...");
		Collections.sort(p1, ClassifierUtils.RACE_TIME_BY_DIVISION);
		Collections.sort(p2, ClassifierUtils.RACE_TIME_BY_DIVISION);
		Collections.sort(p3, ClassifierUtils.RACE_TIME_BY_DIVISION);
		Collections.sort(p4, ClassifierUtils.RACE_TIME_BY_DIVISION);
		Collections.sort(p8, ClassifierUtils.RACE_TIME_BY_DIVISION);
		Collections.sort(p9, ClassifierUtils.RACE_TIME_BY_DIVISION);
		Collections.sort(p15, ClassifierUtils.RACE_TIME_BY_DIVISION);
		Collections.sort(p16, ClassifierUtils.RACE_TIME_BY_DIVISION);
		Collections.sort(p25, ClassifierUtils.RACE_TIME_BY_DIVISION);
		Collections.sort(p26, ClassifierUtils.RACE_TIME_BY_DIVISION);
		ClassifierUtils.updateDataGroups(p1);
		ClassifierUtils.updateDataGroups(p2);
		ClassifierUtils.updateDataGroups(p3);
		ClassifierUtils.updateDataGroups(p4);
		ClassifierUtils.updateDataGroups(p8);
		ClassifierUtils.updateDataGroups(p9);
		ClassifierUtils.updateDataGroups(p15);
		ClassifierUtils.updateDataGroups(p16);
		ClassifierUtils.updateDataGroups(p25);
		ClassifierUtils.updateDataGroups(p26);
		System.out.println("   done!");
		
		System.out.print("[" + new Date() + "] " + "Starting to export data into CSV files...");
		final String filePath = buildFilePath(correctedInputs);
		exportAllManagersCSV(fullRaceResults, filePath);
		exportCSV(p1, filePath, "TempsRéalisésPourFinirP1.csv");
		exportCSV(p3, filePath, "TempsRéalisésPourFinirP3.csv");
		exportCSV(p8, filePath, "TempsRéalisésPourFinirP8.csv");
		exportCSV(p15, filePath, "TempsRéalisésPourFinirP15.csv");
		exportCSV(p25, filePath, "TempsRéalisésPourFinirP25.csv");
		exportCSV(p2, filePath, "TempsMinimumABattrePourFinirP1.csv");
		exportCSV(p4, filePath, "TempsMinimumABattrePourFinirP3.csv");
		exportCSV(p9, filePath, "TempsMinimumABattrePourFinirP8.csv");
		exportCSV(p16, filePath, "TempsMinimumABattrePourFinirP15.csv");
		exportCSV(p26, filePath, "TempsMinimumABattrePourFinirP25.csv");
		System.out.println("   done!");
	}
	
	
	public void exportCSV(final List<RaceResultEntry> listRaceResults, final String filePath, final String fileName) {
		
		final ExporterCSV exporter = new ExporterCSV();
		exporter.setRaceResults(listRaceResults);
		exporter.writeCSV(filePath + fileName);
	}
	
	
	public static void main(final String[] args) {

		final GroupComparator groupComparator = new GroupComparator();
		groupComparator.buildStats(args);
	}
}
