package groupComparator;

public class Manager {
	
	private Integer id;
	private String name;

	public Manager() {
		id = 0;
		name = "";
	}
	
	@Override
	public String toString() {
		return "Manager [id=" + id + ", name=" + name + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
