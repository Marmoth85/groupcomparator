package groupComparator;

public class RaceResultEntry {
	
	private static final String STR_EMPTY_STRING = "";
	
	private Group group;
	private Integer racePosition;
	private String tyres;
	private String country;
	private String raceEvent;
	private Integer numberOfPits;
	private Manager manager;
	private RaceTime raceTime;
	

	public RaceResultEntry() {
		group = new Group();
		racePosition = 0;
		tyres = STR_EMPTY_STRING;
		country = STR_EMPTY_STRING;
		raceEvent = STR_EMPTY_STRING;
		numberOfPits = 0;
		manager = new Manager();
		raceTime = new RaceTime();
	}
	
	
	@Override
	public String toString() {
		return "RaceResultEntry [group=" + group + ", racePosition=" + racePosition + ", manager=" + manager 
				+ ", raceTime=" + raceTime 	+ ", tyres=" + tyres + ", numberOfPits=" + numberOfPits + ", country=" 
				+ country + ", raceEvent=" + raceEvent + "]";
	}


	public RaceTime getRaceTime() {
		return raceTime;
	}


	public void setRaceTime(RaceTime raceTime) {
		this.raceTime = raceTime;
	}


	public Manager getManager() {
		return manager;
	}


	public void setManager(Manager manager) {
		this.manager = manager;
	}


	public Integer getRacePosition() {
		return racePosition;
	}


	public void setRacePosition(Integer racePosition) {
		this.racePosition = racePosition;
	}


	public Group getGroup() {
		return group;
	}


	public void setGroup(Group group) {
		this.group = group;
	}
	
	
	public String getTyres() {
		return tyres;
	}


	public void setTyres(String tyre) {
		this.tyres = tyre;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getRaceEvent() {
		return raceEvent;
	}


	public void setRaceEvent(String raceEvent) {
		this.raceEvent = raceEvent;
	}


	public Integer getNumberOfPits() {
		return numberOfPits;
	}


	public void setNumberOfPits(Integer numberOfPits) {
		this.numberOfPits = numberOfPits;
	}
}
