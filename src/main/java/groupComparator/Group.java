package groupComparator;

public class Group {
	
	private static final String STR_EMPTY_STRING = "";

	enum Division {
		ELITE,
		MASTER,
		PRO,
		AMATEUR,
		ROOKIE
	}
	
	private Division division;
	private Integer levelDivision;
	private Integer groupNumber;
	private String divisionFullName;

	
	public Group() {
		division = null;
		levelDivision = 0;
		groupNumber = 0;
		divisionFullName = STR_EMPTY_STRING;
	}
	
	
	@Override
	public String toString() {
		return "Group [division=" + division + ", levelDivision=" + levelDivision + ", groupNumber=" + groupNumber
				+ ", divisionFullName=" + divisionFullName + "]";
	}


	public Division getDivision() {
		return division;
	}


	public void setDivision(Division division) {
		this.division = division;
	}


	public Integer getLevelDivision() {
		return levelDivision;
	}


	public void setLevelDivision(Integer levelDivision) {
		this.levelDivision = levelDivision;
	}


	public Integer getGroupNumber() {
		return groupNumber;
	}


	public void setGroupNumber(Integer groupNumber) {
		this.groupNumber = groupNumber;
	}


	public String getDivisionFullName() {
		return divisionFullName;
	}


	public void setDivisionFullName(String divisionFullName) {
		this.divisionFullName = divisionFullName;
	}
}
