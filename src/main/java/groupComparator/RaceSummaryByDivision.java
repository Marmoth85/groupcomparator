package groupComparator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import data.importer.RaceResult;
import data.importer.RawRaceResultEntry;
import groupComparator.Group.Division;

public class RaceSummaryByDivision {
	
	private static final String STR_DEFAULT_RELATIVE_RACETIME_STRING = "--";
	private static final String STR_STRING_DNF = "DNF";
	private static final String STR_DELTA_TIME_DELIMITER = "+";
	private static final String STR_EMPTY_STRING = "";
	private static final String STR_FLAG_SPLIT_DELIMITER = "/";
	private static final String STR_GIF_SPLIT_DELIMITER = "\\.";
	private static final String STR_REGEX_STRING_ALL_NUMERIC_CHARACTERS = "[*0-9]";
	private static final int INT_NUMBER_OF_GROUP_IN_SPLIT_STRING = 1;
	private static final int INT_ZERO = 0;
	private static final int INT_DNF_RACE_TIME = 3600 * 4 * 1000;
	
	
	private List<Map<String, List<RawRaceResultEntry>>> rawRaceResults;
	

	public RaceSummaryByDivision() {
		rawRaceResults = new ArrayList<Map<String,List<RawRaceResultEntry>>>();
	}
	
	
	public List<RaceResultEntry> cleanResults() {
		
		final List<RaceResultEntry> raceResults = new ArrayList<>();
		
		for (final Map<String, List<RawRaceResultEntry>> division : rawRaceResults) {
			
			final Set<String> divisionNames = division.keySet();
			
			for (final String divisionName : divisionNames) {
				
				final List<RawRaceResultEntry> groupResult = division.get(divisionName);
				final Group currentGroup = buildGroupCleanEntry(divisionName);
				RaceTime leaderRaceTime = new RaceTime();
				
				for (final RawRaceResultEntry rawRaceEntry : groupResult) {
					
					final RaceResultEntry raceResultEntry = cleanRawEntry(rawRaceEntry);
					raceResultEntry.setGroup(currentGroup);
					
					if (raceResultEntry.getRacePosition() == 1) {
						leaderRaceTime = cleanRawLeaderRaceTime(rawRaceEntry.getRelativeRaceTime());
					}
					
					if (StringUtils.isNotBlank(rawRaceEntry.getRelativeRaceTime())
							&& !rawRaceEntry.getRelativeRaceTime().contains(STR_DELTA_TIME_DELIMITER) 
							&& !leaderRaceTime.getAbsoluteRaceTime().equals(rawRaceEntry.getRelativeRaceTime())) {
						
						// the guy is not leader and has no delta time... Technical problems / DNF expected
						final RaceTime raceTime = buildRaceTimeDNF();
						raceResultEntry.setRaceEvent(rawRaceEntry.getRelativeRaceTime());
						raceResultEntry.setRaceTime(raceTime);
						
					} else {
						
						raceResultEntry.setRaceTime(buildRaceTimeCleanEntry(leaderRaceTime, rawRaceEntry.getRelativeRaceTime()));
					}
					raceResults.add(raceResultEntry);
				}
			}
		}
		
		return raceResults;
	}
	
	
	private RaceTime buildRaceTimeDNF() {
		
		final RaceTime raceTime = new RaceTime();
		
		raceTime.setAbsoluteRaceTimeInMilliseconds(INT_DNF_RACE_TIME);
		raceTime.setRelativeRaceTimeInMilliseconds(INT_ZERO);
		raceTime.setAbsoluteRaceTime(STR_STRING_DNF);
		raceTime.setRelativeRaceTime(STR_STRING_DNF);
		
		return raceTime;
	}
	
	
	private RaceTime cleanRawLeaderRaceTime(final String raceTime) {
		
		final RaceTime leaderRaceTime = new RaceTime();
		
		leaderRaceTime.setRelativeRaceTime(STR_DEFAULT_RELATIVE_RACETIME_STRING);
		leaderRaceTime.setRelativeRaceTimeInMilliseconds(INT_ZERO);
		leaderRaceTime.setAbsoluteRaceTime(raceTime);
		final Integer milliseconds = leaderRaceTime.calculateRaceTimeFromAbsoluteRaceTimeString();
		leaderRaceTime.setAbsoluteRaceTimeInMilliseconds(milliseconds);
		final String absoluteRaceTime = leaderRaceTime.buildCleanRaceTime(milliseconds);
		leaderRaceTime.setAbsoluteRaceTime(absoluteRaceTime);
		
		return leaderRaceTime;
	}

	
	private RaceResultEntry cleanRawEntry(final RawRaceResultEntry rawRaceEntry) {
		
		final RaceResultEntry raceResultEntry = new RaceResultEntry();
		
		raceResultEntry.setRacePosition(buildPositionCleanEntry(rawRaceEntry.getPosition()));
		raceResultEntry.setTyres(rawRaceEntry.getTyres());
		raceResultEntry.setNumberOfPits(buildNumberOfPitsCleanEntry(rawRaceEntry.getPitStops()));
		raceResultEntry.setCountry(buildCountryCleanEntry(rawRaceEntry.getCountry()));
		raceResultEntry.setManager(buildCleanManagerEntry(rawRaceEntry.getIdManager(), rawRaceEntry.getManagerName()));
		
		return raceResultEntry;		
	}
	
	
	private RaceTime buildRaceTimeCleanEntry(final RaceTime leaderRaceTime, final String relativeRaceTime) {
		
		final RaceTime raceTime = new RaceTime();
		
		raceTime.setRelativeRaceTime(relativeRaceTime);
		raceTime.setRelativeRaceTimeInMilliseconds(raceTime.calculateRaceTimeFromRelativeRaceTimeString());
		final Integer milliseconds = leaderRaceTime.getAbsoluteRaceTimeInMilliseconds() + raceTime.getRelativeRaceTimeInMilliseconds();
		raceTime.setAbsoluteRaceTimeInMilliseconds(milliseconds);
		raceTime.setAbsoluteRaceTime(raceTime.buildCleanRaceTime(milliseconds));
		
		return raceTime;
	}
	
	
	private String buildCountryCleanEntry(final String rawCountry) {
		
		String country = STR_EMPTY_STRING;
		
		if (StringUtils.isNotBlank(rawCountry)) {
			
			final String[] urlCountryFlagParts = rawCountry.split(STR_FLAG_SPLIT_DELIMITER);
			
			final int numberOfParts = urlCountryFlagParts.length;
			
			if (urlCountryFlagParts != null && numberOfParts > 1) {
				
				final String gifName = urlCountryFlagParts[numberOfParts - 1];
				
				if (StringUtils.isNotBlank(gifName)) {
					
					final String[] gifParts = gifName.split(STR_GIF_SPLIT_DELIMITER);
					
					if (gifParts != null && gifParts.length > 1) {
						country = gifParts[0];
					}
				}
			}
		}
		
		return country;
	}
	
	
	private Manager buildCleanManagerEntry(final String id, final String rawName) {
		
		final Manager manager = new Manager();
		
		if (StringUtils.isNumeric(id)) {
			manager.setId(Integer.valueOf(id));
		}
		
		if (StringUtils.isNotBlank(rawName) && rawName.length() > 2) {
			// clean the number of won championships in the manager name with replaceAll method
			manager.setName(rawName.substring(2).replaceAll(STR_REGEX_STRING_ALL_NUMERIC_CHARACTERS, STR_EMPTY_STRING));
		}
		
		return manager;
	}
	
	
	private Integer buildNumberOfPitsCleanEntry (final String stops) {
		
		Integer numberOfStops = 0;
		
		if (StringUtils.isNumeric(stops)) {
			numberOfStops = Integer.valueOf(stops);
		}
		
		return numberOfStops;
	}
	
	
	private Integer buildPositionCleanEntry(final String rawPosition) {
		
		Integer position = 0;
		
		if (StringUtils.isNotBlank(rawPosition)) {
			
			final String numericPosition = rawPosition.substring(0, rawPosition.length() - 1);
			
			if (StringUtils.isNotBlank(numericPosition) && StringUtils.isNumeric(numericPosition)) {
				
				position = Integer.valueOf(numericPosition);
			}
		}
		
		return position;
	}
	
	
	private Group buildGroupCleanEntry(final String divisionName) {
		
		final Group group = new Group();
		
		if (StringUtils.isNotBlank(divisionName)) {
			
			if(divisionName.contains(RaceResult.STR_GROUP_NAME_ELITE)) {
				
				group.setDivision(Division.ELITE);
				group.setLevelDivision(1);
				group.setGroupNumber(1);
				group.setDivisionFullName(divisionName);
				
			} else {
				
				if (divisionName.contains(RaceResult.STR_GROUP_NAME_MASTER)) {
					
					group.setDivision(Division.MASTER);
					group.setLevelDivision(2);
					
				} else if (divisionName.contains(RaceResult.STR_GROUP_NAME_PRO)) {
					
					group.setDivision(Division.PRO);
					group.setLevelDivision(3);
					
				} else if (divisionName.contains(RaceResult.STR_GROUP_NAME_AMATEUR)) {
					
					group.setDivision(Division.AMATEUR);
					group.setLevelDivision(4);
					
				} else if (divisionName.contains(RaceResult.STR_GROUP_NAME_ROOKIE)) {
					
					group.setDivision(Division.ROOKIE);
					group.setLevelDivision(5);
					
				}
				
				group.setDivisionFullName(divisionName);
				group.setGroupNumber(getNumberOfGroup(divisionName));
			}
		}
		
		return group;
	}
	
	
	private Integer getNumberOfGroup (final String divisionName) {
		
		final String[] splittedDivisionName = divisionName.split(RaceResult.STR_GROUP_NAME_CLEAN_DELIMITER);
		final String strGroupNumber = splittedDivisionName[INT_NUMBER_OF_GROUP_IN_SPLIT_STRING];
		final Integer groupNumber = Integer.valueOf(strGroupNumber);
		
		return groupNumber;
	}
	
	
	public List<Map<String, List<RawRaceResultEntry>>> getRawRaceResults() {
		return rawRaceResults;
	}

	
	public void setRawRaceResults(List<Map<String, List<RawRaceResultEntry>>> rawRaceResults) {
		this.rawRaceResults = rawRaceResults;
	}
}
