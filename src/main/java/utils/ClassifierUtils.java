package utils;

import java.util.Comparator;
import java.util.List;

import groupComparator.RaceResultEntry;

public class ClassifierUtils {

	public ClassifierUtils() {
	}
	
	public static Comparator<RaceResultEntry> RACE_TIME_BY_DIVISION = new Comparator<RaceResultEntry> () {

		@Override
		public int compare(final RaceResultEntry group1, final RaceResultEntry group2) {
			
			try {
				final Integer division1 = group1.getGroup().getLevelDivision();
				final Integer division2 = group2.getGroup().getLevelDivision();
				
				if (division1 - division2 == 0) {
					
					final Integer raceTime1 = group1.getRaceTime().getAbsoluteRaceTimeInMilliseconds();
					final Integer raceTime2 = group2.getRaceTime().getAbsoluteRaceTimeInMilliseconds();
					
					return raceTime1 - raceTime2;
					
				} else {
					
					return division1 - division2;
				}
			} catch (NullPointerException e) {
				return 0;
			}
		}
	};
	
	public static void updateDataGroups(final List<RaceResultEntry> raceResultList) {
		
		int currentLevelDivision = 0;
		int currentPosition = 0;
		int minRaceTime = Integer.MAX_VALUE;
		int leaderRaceTime = Integer.MAX_VALUE;
		
		for(final RaceResultEntry groupStat : raceResultList) {
			
			if (groupStat.getGroup().getLevelDivision() > currentLevelDivision) {
				currentLevelDivision = groupStat.getGroup().getLevelDivision();
				currentPosition = 1;
				minRaceTime = groupStat.getRaceTime().getAbsoluteRaceTimeInMilliseconds();
				leaderRaceTime = minRaceTime;
			}
			
			int groupDivision = groupStat.getGroup().getLevelDivision();
			int groupRaceTime = groupStat.getRaceTime().getAbsoluteRaceTimeInMilliseconds();
			
			String gap = "";
			
			if (groupDivision == currentLevelDivision) {
				
				if (groupRaceTime != minRaceTime) {
					currentPosition++;
					gap = "+ ";
				}
				
				groupStat.setRacePosition(currentPosition);
				final int raceTimeInMilliseconds = groupRaceTime - leaderRaceTime;
				groupStat.getRaceTime().setRelativeRaceTimeInMilliseconds(raceTimeInMilliseconds);
				final String gapTime = groupStat.getRaceTime().buildCleanRaceTime(raceTimeInMilliseconds);
				groupStat.getRaceTime().setRelativeRaceTime(gap + gapTime);
				minRaceTime = groupRaceTime;
			}
		}
	}
}
