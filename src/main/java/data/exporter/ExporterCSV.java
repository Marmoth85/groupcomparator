package data.exporter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import groupComparator.RaceResultEntry;

public class ExporterCSV {

	private List<RaceResultEntry> raceResults;
	
	public ExporterCSV() {
		raceResults = new ArrayList<>();
	}

	
	private void createDirectoryIfNotExists(final String fileName) {
		final File f = new File(fileName);
		final File parent = f.getParentFile();
		if (!parent.exists()) {
			parent.mkdirs();
		}
	}
	
	
	public void writeAllManagersCSV(final String fileName) {
		
		createDirectoryIfNotExists(fileName);
		
		BufferedWriter bw = null;
		String ligne = "";
		
		try {
			bw = new BufferedWriter(new FileWriter(fileName, false));
			ligne = "Division;Nom du groupe;Echelon de la division;Numéro du groupe;Position;Pays;Manager;Temps de course;Temps en millisecondes;Pneus;Arrêts;Evènement;";
			bw.write(ligne);
			bw.newLine();
			
			for(final RaceResultEntry raceResult : raceResults) {
				ligne = raceResult.getGroup().getDivision() + ";" +
						raceResult.getGroup().getDivisionFullName() + ";" +
						raceResult.getGroup().getLevelDivision() + ";" +
						raceResult.getGroup().getGroupNumber() + ";" +
						raceResult.getRacePosition() + ";" +
						raceResult.getCountry() + ";" +
						raceResult.getManager().getName() + ";" +
						raceResult.getRaceTime().getAbsoluteRaceTime() + ";" +
						raceResult.getRaceTime().getAbsoluteRaceTimeInMilliseconds() + ";" +
						raceResult.getTyres() + ";" +
						raceResult.getNumberOfPits() + ";" +
						raceResult.getRaceEvent() + ";";
				bw.write(ligne);
				bw.newLine();
			}
			
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void writeCSV(final String fileName) {
		
		createDirectoryIfNotExists(fileName);
		
		BufferedWriter bw = null;
		String ligne = "";
		
		try {
			bw = new BufferedWriter(new FileWriter(fileName, false));
			ligne = "Division;Position;Nom du groupe;Temps de course;Ecart;Manager;Pays;Pneus;Arrêts;Evènement;";
			bw.write(ligne);
			bw.newLine();
			
			for(final RaceResultEntry raceResult : raceResults) {
				ligne = raceResult.getGroup().getDivision() + ";" +
						raceResult.getRacePosition() + ";" +
						raceResult.getGroup().getDivisionFullName() + ";" +
						raceResult.getRaceTime().getAbsoluteRaceTime() + ";" +
						raceResult.getRaceTime().getRelativeRaceTime() + ";" +
						raceResult.getManager().getName() + ";" +
						raceResult.getCountry() + ";" +
						raceResult.getTyres() + ";" +
						raceResult.getNumberOfPits() + ";" +
						raceResult.getRaceEvent() + ";";
				bw.write(ligne);
				bw.newLine();
			}
			
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void setRaceResults(List<RaceResultEntry> raceResults) {
		this.raceResults = raceResults;
	}
}
