package data.importer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import groupComparator.GroupComparator;

public class RaceResult {
	
	public static final String STR_GROUP_NAME_ELITE = "Elite";
	public static final String STR_GROUP_NAME_MASTER = "Master";
	public static final String STR_GROUP_NAME_PRO = "Pro";
	public static final String STR_GROUP_NAME_AMATEUR = "Amateur";
	public static final String STR_GROUP_NAME_ROOKIE = "Rookie";
	public static final String STR_GROUP_NAME_CLEAN_DELIMITER = " - ";
	private static final String STR_GROUP_NAME_URL_DELIMITER = "+-+";
	private static final int INT_NUMBER_OF_GROUP_MIN = 1;
	
	private static final String URL_FIRST_PARAMETER_SEASON = "?Season=";
	private static final String URL_PARAMETER_RACE = "&Race=";
	private static final String URL_PARAMETER_GROUP = "&group=";
	
	private static final String ERR_JSOUP_ERROR_CONNECTION = "An error occured with the Jsoup Connexion. See the error message : \n";
	
	private static final String HTML_METADATA_PARSING_SELECT_CRITERIA = "select";
	private static final String HTML_METADATA_PARSING_SELECT_NAME_CRITERIA = "name";
	private static final String HTML_METADATA_PARSING_OPTION_CRITERIA = "option";
	private static final String HTML_METADATA_PARSING_SELECTED_ATTRIBUTE_CRITERIA = "selected";
	private static final String HTML_METADATA_PARSING_OPTION_VALUE_ATTRIBUTE_CRITERIA = "value";
	private static final String HTML_METADATA_PARSING_EXPECTED_SEASON_SELECT_NAME_VALUE = "Season";
	private static final String HTML_METADATA_PARSING_EXPECTED_RACE_SELECT_NAME_VALUE = "Race";
	
	private static final String HTML_RACE_RESULT_PARSING_CRITERIA = "table";
	private static final String HTML_RACE_RESULT_PARSING_CRITERIA_TD = "td";
	private static final String HTML_RACE_RESULT_PARSING_CRITERIA_TR = "tr";
	private static final String HTML_RACE_RESULT_PARSING_POSITION_RAW_DATA_CONTENT = "Pos.";
	private static final String HTML_RACE_RESULT_PARSING_TITLE_ATTRIBUTE_CRITERIA = "title";
	private static final String HTML_RACE_RESULT_PARSING_SRC_ATTRIBUTE_CRITERIA = "src";
	private static final String HTML_RACE_RESULT_PARSING_HREF_ATTRIBUTE_CRITERIA = "href";
	private static final String HTML_RACE_RESULT_PARSING_ID_MANAGER_SPLIT_STRING_REGEX = "=";
	private static final int INT_POSITION_HTML_RACE_RESULT_POSITION = 0;
	private static final int INT_POSITION_HTML_RACE_RESULT_TYRES = 3;
	private static final int INT_POSITION_HTML_RACE_RESULT_COUNTRY = 4;
	private static final int INT_POSITION_HTML_RACE_RESULT_MANAGER = 5;
	private static final int INT_POSITION_HTML_RACE_RESULT_RACE_TIME = 6;
	private static final int INT_POSITION_HTML_RACE_RESULT_COMPLETED_LAPS = 7;
	private static final int INT_POSITION_HTML_RACE_RESULT_COUNT_OF_PIT_STOPS = 8;
	private static final int INT_POSITION_HTML_RACE_RESULT_BEST_LAP = 9;
	private static final int INT_POSITION_HTML_RACE_RESULT_AVERAGE_SPEED = 10;
	private static final int INT_POSITION_ID_MANAGER_IN_SPLITTED_MANAGER_CELL = 1;
	private static final int INT_SIZE_OF_DNF_LINE = 1;
	
	private static final String STR_EMPTY_STRING = "";
	
	private Integer currentSeason;
	private Integer currentRace;

	
	public RaceResult() {
		currentSeason = 0;
		currentRace = 0;
	}
	
	
	public void importCurrentMetaData() {
		
		System.out.print("[" + new Date() + "] " + "Importing current meta data...");
		
		final Connection connexion = Jsoup.connect(GroupComparator.URL_RACE_RESULTS_ROOT);
		connexion.timeout(GroupComparator.INT_JSOUP_CONNEXION_TIMEOUT_LIMIT);
		
		try {
			final Document raceResultWebSheet = connexion.get();
			final Elements selects = raceResultWebSheet.getElementsByTag(HTML_METADATA_PARSING_SELECT_CRITERIA);
			readSelectsContent(selects);
		}
		catch (IOException e) {
			System.err.println(ERR_JSOUP_ERROR_CONNECTION);
			System.err.println(e.getMessage());
		}
		
		System.out.println("   done!");
	}
	
	
	public List<Map<String, List<RawRaceResultEntry>>> importRaceResults(final Integer season, final Integer race, final List<Integer> groups) {
		
		final List<Map<String, List<RawRaceResultEntry>>> rawClassificationsPerLeagueAndGroups = new ArrayList<>();
		
		String divisionURL;
		
		for (int i = 0; i < groups.size(); i++) {
			
			final Integer numberOfGroups = groups.get(i);
			
			switch(i) {
			case 0 :
				divisionURL = STR_GROUP_NAME_ELITE;
				break;
			case 1 : 
				divisionURL = STR_GROUP_NAME_MASTER;
				break;
			case 2 :
				divisionURL = STR_GROUP_NAME_PRO;
				break;
			case 3 :
				divisionURL = STR_GROUP_NAME_AMATEUR;
				break;
			case 4 :
				divisionURL = STR_GROUP_NAME_ROOKIE;
				break;
			default : 
				divisionURL = STR_GROUP_NAME_ELITE;
			}
			
			System.out.print("[" + new Date() + "] " + "Getting data from " + divisionURL + "...");
			
			final Map<String, List<RawRaceResultEntry>> classificationPerGroup = new HashMap<>();
			
			for (Integer groupIndex = INT_NUMBER_OF_GROUP_MIN; groupIndex <= numberOfGroups; ++groupIndex) {
				
				String currentGroupURL = divisionURL;
				String currentGroup = currentGroupURL;
				
				if (!STR_GROUP_NAME_ELITE.equals(divisionURL)) {
					currentGroupURL += STR_GROUP_NAME_URL_DELIMITER + groupIndex;
					currentGroup += STR_GROUP_NAME_CLEAN_DELIMITER + groupIndex;
				}
				
				final String completeURL = GroupComparator.URL_RACE_RESULTS_ROOT 
						+ URL_FIRST_PARAMETER_SEASON + season 
						+ URL_PARAMETER_RACE + race 
						+ URL_PARAMETER_GROUP + currentGroupURL;

				final List<RawRaceResultEntry> rawCopyOfGroup = importRaceResultFromURL(completeURL);
				classificationPerGroup.put(currentGroup, rawCopyOfGroup);
			}
			
			System.out.println("   done!");
			rawClassificationsPerLeagueAndGroups.add(classificationPerGroup);
		}
		
		return rawClassificationsPerLeagueAndGroups;
	}
	
	
	private List<RawRaceResultEntry> importRaceResultFromURL(final String url) {
		
		List<RawRaceResultEntry> groupRawCopyOfClassification = new ArrayList<>();
		
		final Connection connexion = Jsoup.connect(url);
		connexion.timeout(GroupComparator.INT_JSOUP_CONNEXION_TIMEOUT_LIMIT);
		
		try {
			final Document groupeRaceResultWebSheet = connexion.get();
			final Elements tables = groupeRaceResultWebSheet.getElementsByTag(HTML_RACE_RESULT_PARSING_CRITERIA);
			groupRawCopyOfClassification = readTableContent(tables.first());
		}
		catch (IOException e) {
			System.err.println(ERR_JSOUP_ERROR_CONNECTION);
			System.err.println(e.getMessage());
		}
		return groupRawCopyOfClassification;
	}
	
	
	private List<RawRaceResultEntry> readTableContent(final Element table) {
		
		final List<RawRaceResultEntry> groupRawCopyOfClassification = new ArrayList<>();
		
		final Elements linesOfTable = table.getElementsByTag(HTML_RACE_RESULT_PARSING_CRITERIA_TR);
		
		for (final Element line : linesOfTable) {
			
			final Elements cells = line.getElementsByTag(HTML_RACE_RESULT_PARSING_CRITERIA_TD);
			
			final Element cell = cells.first();
			
			if (HTML_RACE_RESULT_PARSING_POSITION_RAW_DATA_CONTENT.equals(cell.text())) {
				
				continue;
				
			} else {
				
				if (cells.size() == INT_SIZE_OF_DNF_LINE) {
					
					continue;
					
				} else {
					
					final RawRaceResultEntry entry = loadEntry(cells);
					groupRawCopyOfClassification.add(entry);
				}
			}
		}
		
		return groupRawCopyOfClassification;
	}
	
	
	private RawRaceResultEntry loadEntry(final Elements cells) {
		
		final String position = getTextContentFromCells(cells, INT_POSITION_HTML_RACE_RESULT_POSITION); 
		final String tyres = getTextContentFromAttributeCells(cells, INT_POSITION_HTML_RACE_RESULT_TYRES, HTML_RACE_RESULT_PARSING_TITLE_ATTRIBUTE_CRITERIA);
		final String country = getTextContentFromAttributeCells(cells, INT_POSITION_HTML_RACE_RESULT_COUNTRY, HTML_RACE_RESULT_PARSING_SRC_ATTRIBUTE_CRITERIA);
		final String manager = getTextContentFromCells(cells, INT_POSITION_HTML_RACE_RESULT_MANAGER); 
		final String idManager = getIdManagerFromAttributeCells(cells, INT_POSITION_HTML_RACE_RESULT_MANAGER, HTML_RACE_RESULT_PARSING_HREF_ATTRIBUTE_CRITERIA);
		final String raceTime = getTextContentFromCells(cells, INT_POSITION_HTML_RACE_RESULT_RACE_TIME);
		final String lapsCompleted = getTextContentFromCells(cells, INT_POSITION_HTML_RACE_RESULT_COMPLETED_LAPS);
		final String pitStops = getTextContentFromCells(cells, INT_POSITION_HTML_RACE_RESULT_COUNT_OF_PIT_STOPS);
		final String bestLap = getTextContentFromCells(cells, INT_POSITION_HTML_RACE_RESULT_BEST_LAP);
		final String averageSpeed = getTextContentFromCells(cells, INT_POSITION_HTML_RACE_RESULT_AVERAGE_SPEED);
		
		final RawRaceResultEntry rawEntry = new RawRaceResultEntry();
		rawEntry.setPosition(position);
		rawEntry.setTyres(tyres);
		rawEntry.setCountry(country);
		rawEntry.setIdManager(idManager);
		rawEntry.setManagerName(manager);
		rawEntry.setRelativeRaceTime(raceTime);
		rawEntry.setLapsCompleted(lapsCompleted);
		rawEntry.setPitStops(pitStops);
		rawEntry.setBestLapTime(bestLap);
		rawEntry.setAverageSpeed(averageSpeed);
		
		return rawEntry;
	}
	
	
	private String getTextContentFromCells(final Elements cells, final int index) {
		
		String textContent = STR_EMPTY_STRING;
		
		if (cells != null && !cells.isEmpty()) {
			
			final Element cell = cells.get(index);
			
			if (cell != null && cell.hasText()) {
				textContent = cell.text();
			}
		}
		
		return textContent;
	}
	
	
	private String getTextContentFromAttributeCells(final Elements cells, final int positionCell, final String attributeName) {
		
		String textContent = STR_EMPTY_STRING;
		
		if (cells != null && !cells.isEmpty()) {
			
			final Element cell = cells.get(positionCell);
			
			if (cell != null) {
				
				final Elements cellsMatchingAttribute = cell.getElementsByAttribute(attributeName);
				
				if (cellsMatchingAttribute != null && !cellsMatchingAttribute.isEmpty()) {
					
					final Element cellMatchingAttribute = cellsMatchingAttribute.first();
					
					if (cellMatchingAttribute != null) {
						textContent = cellMatchingAttribute.attr(attributeName);
					}
				}
			}
		}
		
		return textContent;
	}
	
	
	private String getIdManagerFromAttributeCells(final Elements cells, final int positionCell, final String attributeName) {
		
		String textContent = STR_EMPTY_STRING;
		
		final String attributeTextContent = getTextContentFromAttributeCells(cells, positionCell, attributeName);
		
		if (StringUtils.isNotBlank(attributeTextContent)) {
			
			final String[] stringParts = attributeTextContent.split(HTML_RACE_RESULT_PARSING_ID_MANAGER_SPLIT_STRING_REGEX);
			
			if (stringParts != null && stringParts.length == 2) {
				textContent = stringParts[INT_POSITION_ID_MANAGER_IN_SPLITTED_MANAGER_CELL];
			}
		}
		
		return textContent;
	}
	
	
	private void readSelectsContent(final Elements selects) {
		
		for (final Element select : selects) {
			
			final String selectName = select.attr(HTML_METADATA_PARSING_SELECT_NAME_CRITERIA);
			final Elements options = select.getElementsByTag(HTML_METADATA_PARSING_OPTION_CRITERIA);
			
			for (final Element option : options) {
				
				if (option.hasAttr(HTML_METADATA_PARSING_SELECTED_ATTRIBUTE_CRITERIA)) {
					
					final String optionValue = option.attr(HTML_METADATA_PARSING_OPTION_VALUE_ATTRIBUTE_CRITERIA);
					
					if (HTML_METADATA_PARSING_EXPECTED_SEASON_SELECT_NAME_VALUE.equals(selectName)) {
						
						currentSeason = Integer.valueOf(optionValue);
						
					} else if (HTML_METADATA_PARSING_EXPECTED_RACE_SELECT_NAME_VALUE.equals(selectName)) {
						
						currentRace = Integer.valueOf(optionValue);
					}
					
					break;
				}
			}
		}
	}

	
	public Integer getCurrentSeason() {
		return currentSeason;
	}


	public Integer getCurrentRace() {
		return currentRace;
	}
}
