package data.importer;

public class RawRaceResultEntry {
	
	private String position = null;
	private String tyres = null;
	private String country = null;
	private String idManager = null;
	private String managerName = null;
	private String relativeRaceTime = null;
	private String lapsCompleted = null;
	private String pitStops = null;
	private String bestLapTime = null;
	private String averageSpeed = null;

	public RawRaceResultEntry() {
		super();
	}
	
	@Override
	public String toString() {
		return "RawRaceResultEntry [position=" + position + ", tyres=" + tyres + ", country=" + country + ", idManager="
				+ idManager + ", managerName=" + managerName + ", relativeRaceTime=" + relativeRaceTime
				+ ", lapsCompleted=" + lapsCompleted + ", pitStops=" + pitStops + ", bestLapTime=" + bestLapTime
				+ ", averageSpeed=" + averageSpeed + "]";
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTyres() {
		return tyres;
	}

	public void setTyres(String types) {
		this.tyres = types;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getIdManager() {
		return idManager;
	}

	public void setIdManager(String idManager) {
		this.idManager = idManager;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public String getRelativeRaceTime() {
		return relativeRaceTime;
	}

	public void setRelativeRaceTime(String relativeRaceTime) {
		this.relativeRaceTime = relativeRaceTime;
	}

	public String getLapsCompleted() {
		return lapsCompleted;
	}

	public void setLapsCompleted(String lapsCompleted) {
		this.lapsCompleted = lapsCompleted;
	}

	public String getPitStops() {
		return pitStops;
	}

	public void setPitStops(String pitStops) {
		this.pitStops = pitStops;
	}

	public String getBestLapTime() {
		return bestLapTime;
	}

	public void setBestLapTime(String bestLapTime) {
		this.bestLapTime = bestLapTime;
	}

	public String getAverageSpeed() {
		return averageSpeed;
	}

	public void setAverageSpeed(String meanSpeed) {
		this.averageSpeed = meanSpeed;
	}
}
