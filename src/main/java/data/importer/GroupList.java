package data.importer;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import exceptions.QualificationsWebSheetParsingException;
import groupComparator.GroupComparator;

public class GroupList {
	
	private static final String HTML_CONTAINER_PARSING_CRITERIA = "table";
	private static final String HTML_CONTAINER_DISCRIMINATION_CRITERIA = "class";
	private static final String HTML_CONTAINER_DISCRIMINATION_EXPECTED_VALUE = "center";
	private static final String HTML_CONTENT_CRITERIA = "script";
	private static final String HTML_ELITE_CRITERIA = "Elite";
	private static final String HTML_MASTER_CRITERIA = "Master";
	private static final String HTML_PRO_CRITERIA = "Pro";
	private static final String HTML_AMATEUR_CRITERIA = "Amateur";
	private static final String HTML_ROOKIE_CRITERIA = "Rookie";
	
	
	private int countOfEliteGroup;
	private int countOfMasterGroup;
	private int countOfProGroup;
	private int countOfAmateurGroup;
	private int countOfRookieGroup;

	
	public GroupList() {
		
		countOfEliteGroup = 0;
		countOfMasterGroup = 0;
		countOfProGroup = 0;
		countOfAmateurGroup = 0;
		countOfRookieGroup = 0;
		
	}
	
	
	public void importNumberOfGroupsByLeague() {
		
		final Connection connexion = Jsoup.connect(GroupComparator.URL_QUALIFICATIONS_STATISTICS);
		connexion.timeout(GroupComparator.INT_JSOUP_CONNEXION_TIMEOUT_LIMIT);
		
		try {
			final Document statsQualifsWebSheet = connexion.get();
			final Elements tables = statsQualifsWebSheet.getElementsByTag(HTML_CONTAINER_PARSING_CRITERIA);
			final Element table = selectTableOfGroupsFromWebSheet(tables);
			readTableContent(table);
		}
		catch (IOException e) {
			System.err.println("An error occured with the Jsoup Connexion. See the error message : \n");
			System.err.println(e.getMessage());
		}
		catch(QualificationsWebSheetParsingException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	
	private Element selectTableOfGroupsFromWebSheet(final Elements tables) throws QualificationsWebSheetParsingException {
		
		Element groupsByLeagueTable = null;
		
		for (final Element table : tables) {
			
			final Attributes tableAttributes = table.attributes();
			final String cssClassTable = tableAttributes.get(HTML_CONTAINER_DISCRIMINATION_CRITERIA);
			
			if (HTML_CONTAINER_DISCRIMINATION_EXPECTED_VALUE.equals(cssClassTable)) {
				groupsByLeagueTable = table;
				break;
			}
		}
		
		if (groupsByLeagueTable == null) {
			throw new QualificationsWebSheetParsingException("[ERROR MESSAGE] A problem occured with Jsoup getting data from the GPRO qualifications statistics web sheet. \n"
					+ "[ERROR MESSAGE] The expected html table of data has not been found or loaded.\n"
					+ "[ERROR MESSAGE] The program can not go further and have to be stopped right now.");
		}
		
		return groupsByLeagueTable;
	}
	
	
	private void readTableContent (final Element table) {
		
		final Elements groups = table.getElementsByTag(HTML_CONTENT_CRITERIA);
		
		for (final Element group : groups) {
			
			final String contentLine = group.data();
			
			if (StringUtils.isNotBlank(contentLine)) {
				
				if (contentLine.contains(HTML_ELITE_CRITERIA)) {
					++countOfEliteGroup;
					continue;
				}
				if (contentLine.contains(HTML_MASTER_CRITERIA)) {
					++countOfMasterGroup;
					continue;
				}
				if (contentLine.contains(HTML_PRO_CRITERIA)) {
					++countOfProGroup;
					continue;
				}
				if (contentLine.contains(HTML_AMATEUR_CRITERIA)) {
					++countOfAmateurGroup;
					continue;
				}
				if (contentLine.contains(HTML_ROOKIE_CRITERIA)) {
					++countOfRookieGroup;
				}
			}
		}
	}

	
	public int getCountOfEliteGroup() {
		return countOfEliteGroup;
	}

	public int getCountOfMasterGroup() {
		return countOfMasterGroup;
	}

	public int getCountOfProGroup() {
		return countOfProGroup;
	}

	public int getCountOfAmateurGroup() {
		return countOfAmateurGroup;
	}

	public int getCountOfRookieGroup() {
		return countOfRookieGroup;
	}
}